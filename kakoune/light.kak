source "%val{config}/colors/base.kak"

face global comment bright-black
face global meta black

face global SecondarySelection default,bright-white+fg
face global SecondaryCursor bright-black+rfg
face global SecondaryCursorEol bright-black+rfg

face global MenuForeground bright-black+r
face global MenuBackground default,bright-white
face global MenuInfo bright-black
face global Information default,bright-white

face global LineNumbers white
face global LineNumbersWrapped bright-white
face global LineNumberCursor bright-black

face global MatchingChar default,bright-white+f
face global Whitespace white+f
face global BufferPadding white
