source "%val{config}/colors/base.kak"

face global comment bright-white
face global meta white

face global SecondarySelection default,black+fg
face global SecondaryCursor white+rfg
face global SecondaryCursorEol white+rfg

face global MenuForeground white+r
face global MenuBackground default,bright-black
face global MenuInfo bright-white
face global Information default,bright-black

face global LineNumbers bright-white
face global LineNumbersWrapped black
face global LineNumberCursor white

face global MatchingChar default,black+f
face global Whitespace black+f
face global BufferPadding black
