# theme

Light and dark colorscheme for use with terminals.

## screenshots

![terminal](./_img/term.png)

![kakoune](./_img/kakoune.png)
