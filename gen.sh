#!/usr/bin/env dash

gen() {
  echo "background: &background '#$background'
foreground: &foreground '#$foreground'
black: &black '#$black'
red: &red '#$red'
green: &green '#$green'
yellow: &yellow '#$yellow'
blue: &blue '#$blue'
magenta: &magenta '#$magenta'
cyan: &cyan '#$cyan'
white: &white '#$white'
bright_black: &bright_black '#$bright_black'
bright_red: &bright_red '#$bright_red'
bright_green: &bright_green '#$bright_green'
bright_yellow: &bright_yellow '#$bright_yellow'
bright_blue: &bright_blue '#$bright_blue'
bright_magenta: &bright_magenta '#$bright_magenta'
bright_cyan: &bright_cyan '#$bright_cyan'
bright_white: &bright_white '#$bright_white'
selection_background: &selection_background '#$selection_background'
$(cat alacritty/base.yml)" > "alacritty/$1.yml"

  echo "local background='$background'
local foreground='$foreground'
local black='$black'
local red='$red'
local green='$green'
local yellow='$yellow'
local blue='$blue'
local magenta='$magenta'
local cyan='$cyan'
local white='$white'
local bright_black='$bright_black'
local bright_red='$bright_red'
local bright_green='$bright_green'
local bright_yellow='$bright_yellow'
local bright_blue='$bright_blue'
local bright_magenta='$bright_magenta'
local bright_cyan='$bright_cyan'
local bright_white='$bright_white'
local selection_background='$selection_background'
$(cat wezterm/base.lua)" > "wezterm/$1.lua"
}

background=1f2025
foreground=dcdde3
black=4b5164
red=f19581
green=9ad777
yellow=f2c388
blue=85c8f3
magenta=eda1de
cyan=7fd4d9
white=a9aec1
bright_black=2b2f3a
bright_red=703441
bright_green=2c5433
bright_yellow=6f4830
bright_blue=354c70
bright_magenta=613c63
bright_cyan=2e5667
bright_white=6f7384
selection_background="$bright_black"
gen dark

background=f8f8f8
foreground=4a4d59
black=6b6e79
red=e06c63
green=6cab5e
yellow=dea664
blue=589ade
magenta=c064b6
cyan=48a8c0
white=aeb1be
bright_black=818496
bright_red=eebfb2
bright_green=c1deb2
bright_yellow=f0ddb1
bright_blue=b6d4f2
bright_magenta=eac6e1
bright_cyan=b6e3e7
bright_white=dadce5
selection_background="$bright_white"
gen light
