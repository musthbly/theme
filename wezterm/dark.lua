local background='1f2025'
local foreground='dcdde3'
local black='4b5164'
local red='f19581'
local green='9ad777'
local yellow='f2c388'
local blue='85c8f3'
local magenta='eda1de'
local cyan='7fd4d9'
local white='a9aec1'
local bright_black='2b2f3a'
local bright_red='703441'
local bright_green='2c5433'
local bright_yellow='6f4830'
local bright_blue='354c70'
local bright_magenta='613c63'
local bright_cyan='2e5667'
local bright_white='6f7384'
local selection_background='2b2f3a'
return {
  background = background,
  foreground = foreground,
  cursor_bg = foreground,
  cursor_border = foreground,
  cursor_fg = background,
  selection_bg = selection_background,
  selection_fg = foreground,
  scrollbar_thumb = selection_background,
  ansi = {
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white
  },
  brights = {
    bright_black,
    bright_red,
    bright_green,
    bright_yellow,
    bright_blue,
    bright_magenta,
    bright_cyan,
    bright_white
  }
}
