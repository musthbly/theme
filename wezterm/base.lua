return {
  background = background,
  foreground = foreground,
  cursor_bg = foreground,
  cursor_border = foreground,
  cursor_fg = background,
  selection_bg = selection_background,
  selection_fg = foreground,
  scrollbar_thumb = selection_background,
  ansi = {
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white
  },
  brights = {
    bright_black,
    bright_red,
    bright_green,
    bright_yellow,
    bright_blue,
    bright_magenta,
    bright_cyan,
    bright_white
  }
}
