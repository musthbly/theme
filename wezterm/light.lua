local background='f8f8f8'
local foreground='4a4d59'
local black='6b6e79'
local red='e06c63'
local green='6cab5e'
local yellow='dea664'
local blue='589ade'
local magenta='c064b6'
local cyan='48a8c0'
local white='aeb1be'
local bright_black='818496'
local bright_red='eebfb2'
local bright_green='c1deb2'
local bright_yellow='f0ddb1'
local bright_blue='b6d4f2'
local bright_magenta='eac6e1'
local bright_cyan='b6e3e7'
local bright_white='dadce5'
local selection_background='dadce5'
return {
  background = background,
  foreground = foreground,
  cursor_bg = foreground,
  cursor_border = foreground,
  cursor_fg = background,
  selection_bg = selection_background,
  selection_fg = foreground,
  scrollbar_thumb = selection_background,
  ansi = {
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white
  },
  brights = {
    bright_black,
    bright_red,
    bright_green,
    bright_yellow,
    bright_blue,
    bright_magenta,
    bright_cyan,
    bright_white
  }
}
